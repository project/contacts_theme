<?php

namespace Drupal\contacts_theme;

use Drupal\Core\Render\Element;
use Drupal\Core\Security\TrustedCallbackInterface;

/**
 * Pre-render callbacks for contacts_theme.
 *
 * @package Drupal\contacts_theme
 */
class PreRenderCallbacks implements TrustedCallbackInterface {

  /**
   * {@inheritdoc}
   */
  public static function trustedCallbacks() {
    return [
      'compositePreRender',
      'preRenderActions',
      'preRenderIgnoreEmpty',
      'propagateColumns',
    ];
  }

  /**
   * Pre-render to make composite children not form groups..
   */
  public static function compositePreRender($element) {
    foreach (Element::children($element) as $child) {
      $element[$child]['#form_group'] = FALSE;
    }
    return $element;
  }

  /**
   * Pre render callback for the actions element.
   */
  public static function preRenderActions($element) {
    // Align actions with the main column.
    if (!empty($element['#form_columns'])) {
      $element['#prefix'] = '<div class="row">';
      $element['#suffix'] = '</div>';

      $classes = $element['#form_columns']['input'];
      foreach ($element['#form_columns']['label'] as $class) {
        $classes[] = str_replace('col-', 'offset-', $class);
      }

      $element['#attributes'] += ['class' => []];
      $element['#attributes']['class'] = array_merge($element['#attributes']['class'], $classes);
    }

    return $element;
  }

  /**
   * Pre-render callback for containers to prevent rendering empty elements.
   */
  public static function preRenderIgnoreEmpty($element) {
    // If already printed or access is FALSE, we don't need to do any more.
    if (!empty($element['#printed']) || !contacts_theme_check_access($element)) {
      return $element;
    }

    // Look for any accessible children.
    $has_children = FALSE;
    foreach (Element::children($element) as $key) {
      if (contacts_theme_check_access($element[$key])) {
        $has_children = TRUE;
        break;
      }
    }

    // Prevent rendering if there are no accessible children.
    if (!$has_children) {
      $element['#printed'] = TRUE;
    }

    return $element;
  }

  /**
   * Pre render callback to propagate form column settings.
   *
   * @param array $element
   *   The element array.
   * @param array|null $column_settings
   *   @internal The column settings to propagate.
   *
   * @return array
   *   The adjusted element.
   */
  public static function propagateColumns(array $element, $column_settings = NULL) {
    // If not set, we either propagate our given value
    // or there is nothing to do.
    if (!isset($element['#form_columns'])) {
      if (isset($column_settings)) {
        $element['#form_columns'] = $column_settings;
      }
      else {
        return $element;
      }
    }
    // Otherwise, process the form columns, merging in the defaults.
    elseif ($element['#form_columns']) {
      // Convert non arrays to an empty array.
      if (!is_array($element['#form_columns'])) {
        $element['#form_columns'] = [];
      }

      // Add our default settings.
      // phpcs:disable Drupal.Arrays.Array.LongLineDeclaration
      $element['#form_columns'] += [
        'label' => ['col-sm-5', 'col-md-4', 'col-lg-4', 'col-xl-3'],
        'input' => ['col-sm-7', 'col-md-7', 'col-lg-8', 'col-xl-6'],
        'errors' => ['col-md-8', 'col-lg-8', 'col-xl-4', 'offset-md-4', 'offset-lg-3', 'offset-xl-0'],
        'description' => ['col-md-8', 'col-lg-8', 'col-xl-9', 'offset-md-4', 'offset-lg-4', 'offset-xl-3'],
      ];
      // phpcs:enable

      // Default the offset classes to line up with the input.
      if (!isset($element['#form_columns']['_offset'])) {
        $element['#form_columns']['_offset'] = array_map(function ($class) {
          return str_replace('col-', 'offset-', $class);
        }, $element['#form_columns']['label']);
      }

      // If description or error are FALSE, we use the input/offset classes.
      $classes = array_merge($element['#form_columns']['input'], $element['#form_columns']['_offset'], ['flex-last']);
      if (!$element['#form_columns']['description']) {
        $element['#form_columns']['description'] = $classes;
      }
      if (!$element['#form_columns']['errors']) {
        $element['#form_columns']['errors'] = $classes;
      }
    }

    // Propagate to our children.
    foreach (Element::children($element) as $child) {
      $element[$child] = self::propagateColumns($element[$child], $element['#form_columns']);
    }

    return $element;
  }

}
